/*
 * 002.c
 *
 *  Created on: Oct 8, 2018
 *      Author: luiz
 */

#include "002.h"

void problem002() {
	long int i = 0;
	long int sum = 0;
	long int num = 0;
	while (num <= 4000000) {
		if (is_multiple(num, 2))
			sum += num;

		num = fibonacci(i);
		i++;
	}

	printf("%ld", sum);
}
