/*
 * 001.h
 *
 *  Created on: Oct 8, 2018
 *      Author: luiz
 */

#ifndef PROBLEMS_001_H_
#define PROBLEMS_001_H_

#include "../std.h"



//If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.
//Find the sum of all the multiples of 3 or 5 below 1000.

void problem001 ();

#endif /* PROBLEMS_001_H_ */
