/*
 * 001.c
 *
 *  Created on: Oct 8, 2018
 *      Author: luiz
 */

#include "001.h"

void problem001 () {
	long int sum = 0;
	long int i;

	for (i=1; i<1000; i++) {
		if (is_multiple(i, 3) || is_multiple(i, 5)) {
			sum += i;
		}
	}

	printf("%ld\n", sum);
}
