/*
 * std.c
 *
 *  Created on: Oct 8, 2018
 *      Author: luiz
 */

#include "std.h"

boolean is_multiple (const long int dividend, const long int divisor)
{

	if (0 >= divisor) {
		return FALSE;
	}

	if (0 == dividend%divisor) {
		return TRUE;
	}
	return FALSE;
}

long int fibonacci (const long int n) {
	if (n<=0) {
		return 1;
	}

	return fibonacci(n-1) + fibonacci(n-2);
}
