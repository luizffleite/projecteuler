/*
 * std.h
 *
 *  Created on: Oct 8, 2018
 *      Author: luiz
 */

#ifndef STD_H_
#define STD_H_

#include <stdio.h>
#include <stdlib.h>

typedef enum {FALSE, TRUE} boolean;

boolean is_multiple (const long int dividend, const long int divisor);

long int fibonacci (const long int n);
#endif /* STD_H_ */
